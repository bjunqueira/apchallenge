'use strict';



;define("apchallenge-ember/adapters/employee", ["exports", "ember-data"], function (_exports, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = _emberData.default.RESTAdapter.extend({
    host: 'http://127.0.0.1:8000',
    namespace: 'api/v1',

    pathForTypes() {
      return 'employees';
    }

  });

  _exports.default = _default;
});
;define("apchallenge-ember/app", ["exports", "apchallenge-ember/resolver", "ember-load-initializers", "apchallenge-ember/config/environment"], function (_exports, _resolver, _emberLoadInitializers, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  const App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });
  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);
  var _default = App;
  _exports.default = _default;
});
;define("apchallenge-ember/components/card-handler", ["exports", "jquery"], function (_exports, _jquery) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Component.extend({
    didReceiveAttrs(options) {
      this._super(...arguments); // Check if employee has a manager and manager has already a container rendered.


      if (this.employee.manager_id == undefined || this.employee.manager_id != undefined && (0, _jquery.default)('li[data-id=' + this.employee.manager_id + ']').length) {
        // Found a container for the manager, don't create a new one.
        this.create_manager_card = false;
      } else {
        this.create_manager_card = true;
      }
    },

    didInsertElement() {
      this._super(...arguments); // If card already was created, we insert the current employee in it.


      if (this.create_manager_card == false) {
        // Manager card is already created. Move employee card inside id.
        if (this.employee.manager_id != undefined) {
          let employeeCard = (0, _jquery.default)('li[data-id=' + this.employee.id + ']').closest('ol');
          let managerCard = (0, _jquery.default)('li[data-id=' + this.employee.manager_id + ']');
          (0, _jquery.default)(managerCard).first().append(employeeCard);
        }
      }
    }

  });

  _exports.default = _default;
});
;define("apchallenge-ember/components/employee-card", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Component.extend({});

  _exports.default = _default;
});
;define("apchallenge-ember/components/employees-tree", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Component.extend({
    store: Ember.inject.service(),

    didRender() {
      this._super(...arguments);

      var store = this.get('store');
      jQuery('.sortable').nestedSortable({
        forcePlaceholderSize: true,
        handle: 'div',
        helper: 'clone',
        items: 'li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        isTree: false,
        expandOnHover: 700,
        update: function (event, ui) {
          let employeeId = jQuery(ui.item[0]).attr('data-id');
          let newManagerId = jQuery(ui.item[0]).closest('ol').closest('li').attr('data-id');
          store.findRecord('employee', employeeId).then(function (employee) {
            Ember.set(employee, "manager_id", newManagerId);
            employee.save();
          });
        }
      });
    }

  });

  _exports.default = _default;
});
;define("apchallenge-ember/components/list-filter", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Component.extend({
    classNames: ['list-filter'],
    value: '',

    init() {
      this._super(...arguments);

      this.filter('').then(allResults => {
        this.set('results', allResults.results);
      });
    },

    actions: {
      handleFilterEntry() {
        let filterInputValue = this.value; // The input value.

        let filterAction = this.filter;
        filterAction(filterInputValue).then(filterResults => {
          if (filterResults.query === this.value) {
            this.set('results', filterResults.results);
          }
        });
      }

    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/components/sortable-js", ["exports", "ember-sortablejs/components/sortable-js"], function (_exports, _sortableJs) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _sortableJs.default;
    }
  });
});
;define("apchallenge-ember/components/welcome-page", ["exports", "ember-welcome-page/components/welcome-page"], function (_exports, _welcomePage) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
;define("apchallenge-ember/controllers/employees", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Controller.extend({
    actions: {
      filterEmployees(param) {
        if (param !== '') {
          return this.store.query('employee', {
            filter: param
          }).then(function (results) {
            return {
              query: param,
              results: results
            };
          });
        } else {
          return this.store.findAll('employee').then(function (results) {
            return {
              query: param,
              results: results
            };
          });
        }
      }

    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/helpers/app-version", ["exports", "apchallenge-ember/config/environment", "ember-cli-app-version/utils/regexp"], function (_exports, _environment, _regexp) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.appVersion = appVersion;
  _exports.default = void 0;

  function appVersion(_, hash = {}) {
    const version = _environment.default.APP.version; // e.g. 1.0.0-alpha.1+4jds75hf
    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility

    let versionOnly = hash.versionOnly || hash.hideSha;
    let shaOnly = hash.shaOnly || hash.hideVersion;
    let match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      } // Fallback to just version


      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  var _default = Ember.Helper.helper(appVersion);

  _exports.default = _default;
});
;define("apchallenge-ember/helpers/pluralize", ["exports", "ember-inflector/lib/helpers/pluralize"], function (_exports, _pluralize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _pluralize.default;
  _exports.default = _default;
});
;define("apchallenge-ember/helpers/singularize", ["exports", "ember-inflector/lib/helpers/singularize"], function (_exports, _singularize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _singularize.default;
  _exports.default = _default;
});
;define("apchallenge-ember/initializers/app-version", ["exports", "ember-cli-app-version/initializer-factory", "apchallenge-ember/config/environment"], function (_exports, _initializerFactory, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  let name, version;

  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  var _default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
  _exports.default = _default;
});
;define("apchallenge-ember/initializers/container-debug-adapter", ["exports", "ember-resolver/resolvers/classic/container-debug-adapter"], function (_exports, _containerDebugAdapter) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = {
    name: 'container-debug-adapter',

    initialize() {
      let app = arguments[1] || arguments[0];
      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }

  };
  _exports.default = _default;
});
;define("apchallenge-ember/initializers/ember-data", ["exports", "ember-data/setup-container", "ember-data"], function (_exports, _setupContainer, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
  
    This code initializes Ember-Data onto an Ember application.
  
    If an Ember.js developer defines a subclass of DS.Store on their application,
    as `App.StoreService` (or via a module system that resolves to `service:store`)
    this code will automatically instantiate it and make it available on the
    router.
  
    Additionally, after an application's controllers have been injected, they will
    each have the store made available to them.
  
    For example, imagine an Ember.js application with the following classes:
  
    ```app/services/store.js
    import DS from 'ember-data';
  
    export default DS.Store.extend({
      adapter: 'custom'
    });
    ```
  
    ```app/controllers/posts.js
    import { Controller } from '@ember/controller';
  
    export default Controller.extend({
      // ...
    });
  
    When the application is initialized, `ApplicationStore` will automatically be
    instantiated, and the instance of `PostsController` will have its `store`
    property set to that instance.
  
    Note that this code will only be run if the `ember-application` package is
    loaded. If Ember Data is being used in an environment other than a
    typical application (e.g., node.js where only `ember-runtime` is available),
    this code will be ignored.
  */
  var _default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
  _exports.default = _default;
});
;define("apchallenge-ember/initializers/export-application-global", ["exports", "apchallenge-ember/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.initialize = initialize;
  _exports.default = void 0;

  function initialize() {
    var application = arguments[1] || arguments[0];

    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;

      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;
        application.reopen({
          willDestroy: function () {
            this._super.apply(this, arguments);

            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  var _default = {
    name: 'export-application-global',
    initialize: initialize
  };
  _exports.default = _default;
});
;define("apchallenge-ember/instance-initializers/ember-data", ["exports", "ember-data/initialize-store-service"], function (_exports, _initializeStoreService) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = {
    name: 'ember-data',
    initialize: _initializeStoreService.default
  };
  _exports.default = _default;
});
;define("apchallenge-ember/models/employee", ["exports", "ember-data"], function (_exports, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  const {
    Model,
    attr
  } = _emberData.default;

  var _default = Model.extend({
    manager_id: attr('string'),
    name: attr('string'),
    phone: attr('string'),
    mobile: attr('string'),
    email: attr('string'),
    picture: attr('string'),
    address: attr('string')
  });

  _exports.default = _default;
});
;define("apchallenge-ember/resolver", ["exports", "ember-resolver"], function (_exports, _emberResolver) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _emberResolver.default;
  _exports.default = _default;
});
;define("apchallenge-ember/router", ["exports", "apchallenge-ember/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  const Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });
  Router.map(function () {
    this.route('employees');
  });
  var _default = Router;
  _exports.default = _default;
});
;define("apchallenge-ember/routes/employees", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Route.extend({
    model() {
      return this.store.findAll('employee');
    }

  });

  _exports.default = _default;
});
;define("apchallenge-ember/serializers/employee", ["exports", "ember-data"], function (_exports, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = _emberData.default.RESTSerializer.extend({
    normalizeResponse(store, primaryModelClass, payload, id, requestType) {
      payload = {
        employees: payload
      };
      return this._super(store, primaryModelClass, payload, id, requestType);
    }

  });

  _exports.default = _default;
});
;define("apchallenge-ember/services/ajax", ["exports", "ember-ajax/services/ajax"], function (_exports, _ajax) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
;define("apchallenge-ember/templates/application", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "4gfUDGVY",
    "block": "{\"symbols\":[],\"statements\":[[1,[23,\"outlet\"],false],[0,\"\\n\"]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/application.hbs"
    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/templates/components/card-handler", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "D1JhCZNF",
    "block": "{\"symbols\":[],\"statements\":[[4,\"if\",[[24,0,[\"create_manager_card\"]]],null,{\"statements\":[[0,\"  \"],[7,\"ol\"],[9],[0,\"\\n    \"],[7,\"li\"],[12,\"data-id\",[30,[[24,0,[\"employee\",\"manager_id\"]]]]],[9],[0,\"\\n      \"],[7,\"div\"],[11,\"class\",\"handler\"],[9],[10],[0,\"\\n      \"],[7,\"ol\"],[9],[0,\"\\n        \"],[5,\"employee-card\",[],[[\"@employee\"],[[23,\"employee\"]]],{\"statements\":[],\"parameters\":[]}],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"  \"],[7,\"ol\"],[9],[0,\"\\n    \"],[5,\"employee-card\",[],[[\"@employee\"],[[23,\"employee\"]]],{\"statements\":[],\"parameters\":[]}],[0,\"\\n  \"],[10],[0,\"\\n\"]],\"parameters\":[]}]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/components/card-handler.hbs"
    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/templates/components/employee-card", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "2zfuiIwB",
    "block": "{\"symbols\":[],\"statements\":[[7,\"li\"],[11,\"class\",\"employee-card\"],[12,\"data-id\",[30,[[24,0,[\"employee\",\"id\"]]]]],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"level-ajustable col-md-4\"],[9],[0,\"\\n      \"],[7,\"div\"],[11,\"class\",\"col-md-4\"],[9],[0,\"\\n        \"],[7,\"img\"],[11,\"class\",\"avatar\"],[12,\"src\",[30,[\"data:image/jpeg;base64,\",[24,0,[\"employee\",\"picture\"]]]]],[11,\"alt\",\"Employee Picture\"],[9],[10],[0,\"\\n      \"],[10],[0,\"\\n      \"],[7,\"div\"],[11,\"class\",\"col-md-8\"],[9],[0,\"\\n        \"],[7,\"p\"],[9],[1,[24,0,[\"employee\",\"name\"]],false],[10],[0,\"\\n        \"],[7,\"p\"],[9],[1,[24,0,[\"employee\",\"email\"]],false],[10],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"col-md-4 phone\"],[9],[0,\"\\n      \"],[7,\"p\"],[9],[1,[24,0,[\"employee\",\"phone\"]],false],[10],[0,\"\\n      \"],[7,\"p\"],[9],[1,[24,0,[\"employee\",\"mobile\"]],false],[10],[0,\"\\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"],[10],[0,\"\\n\"]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/components/employee-card.hbs"
    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/templates/components/employees-tree", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "yetLIGLS",
    "block": "{\"symbols\":[\"employee\"],\"statements\":[[7,\"ol\"],[11,\"class\",\"sortable\"],[9],[0,\"\\n  \"],[7,\"li\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"handler\"],[9],[10],[0,\"\\n\"],[4,\"each\",[[24,0,[\"employees\"]]],null,{\"statements\":[[0,\"      \"],[5,\"card-handler\",[],[[\"@employee\"],[[24,1,[]]]],{\"statements\":[],\"parameters\":[]}],[0,\"\\n\"]],\"parameters\":[1]},null],[0,\"  \"],[10],[0,\"\\n\"],[10],[0,\"\\n\"]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/components/employees-tree.hbs"
    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/templates/components/list-filter", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "bcmVAW0h",
    "block": "{\"symbols\":[\"&default\"],\"statements\":[[1,[29,\"input\",null,[[\"value\",\"key-up\",\"class\",\"placeholder\"],[[24,0,[\"value\"]],[29,\"action\",[[24,0,[]],\"handleFilterEntry\"],null],\"light\",\"Search name, email, etc.\"]]],false],[0,\"\\n\"],[15,1,[[24,0,[\"results\"]]]],[0,\"\\n\"]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/components/list-filter.hbs"
    }
  });

  _exports.default = _default;
});
;define("apchallenge-ember/templates/employees", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "fQBB3vbS",
    "block": "{\"symbols\":[\"filteredResults\"],\"statements\":[[5,\"list-filter\",[],[[\"@filter\"],[[29,\"action\",[[24,0,[]],\"filterEmployees\"],null]]],{\"statements\":[[0,\"\\n\"],[2,\" TODO add handler for empty search results \"],[0,\"\\n  \"],[5,\"employees-tree\",[],[[\"@employees\"],[[24,1,[]]]],{\"statements\":[],\"parameters\":[]}],[0,\"\\n\\n\"]],\"parameters\":[1]}],[0,\"\\n\\n  \"],[2,\" TODO show more when scrolling. \"],[0,\"\\n\"],[1,[23,\"outlet\"],false],[0,\"\\n\"]],\"hasEval\":false}",
    "meta": {
      "moduleName": "apchallenge-ember/templates/employees.hbs"
    }
  });

  _exports.default = _default;
});
;

;define('apchallenge-ember/config/environment', [], function() {
  var prefix = 'apchallenge-ember';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

;
          if (!runningTests) {
            require("apchallenge-ember/app")["default"].create({"name":"apchallenge-ember","version":"0.0.0"});
          }
        
//# sourceMappingURL=apchallenge-ember.map
