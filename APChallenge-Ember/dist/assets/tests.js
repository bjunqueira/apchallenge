'use strict';

define("apchallenge-ember/tests/integration/components/card-handler-test", ["qunit", "ember-qunit", "@ember/test-helpers"], function (_qunit, _emberQunit, _testHelpers) {
  "use strict";

  (0, _qunit.module)('Integration | Component | card-handler', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "d7eDUqxp",
        "block": "{\"symbols\":[],\"statements\":[[5,\"card-handler\",[],[[],[]]]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "1/2Mmrha",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"card-handler\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("apchallenge-ember/tests/integration/components/employee-card-test", ["qunit", "ember-qunit", "@ember/test-helpers"], function (_qunit, _emberQunit, _testHelpers) {
  "use strict";

  (0, _qunit.module)('Integration | Component | employee-card', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "kIcma0Hp",
        "block": "{\"symbols\":[],\"statements\":[[5,\"employee-card\",[],[[],[]]]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "QrKeNeS6",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"employee-card\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("apchallenge-ember/tests/integration/components/employees-tree-test", ["qunit", "ember-qunit", "@ember/test-helpers"], function (_qunit, _emberQunit, _testHelpers) {
  "use strict";

  (0, _qunit.module)('Integration | Component | employees-tree', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "84jHTSU6",
        "block": "{\"symbols\":[],\"statements\":[[5,\"employees-tree\",[],[[],[]]]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "zFby0qP3",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"employees-tree\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("apchallenge-ember/tests/integration/components/list-filter-test", ["qunit", "ember-qunit", "@ember/test-helpers"], function (_qunit, _emberQunit, _testHelpers) {
  "use strict";

  (0, _qunit.module)('Integration | Component | list-filter', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "Omjwu85K",
        "block": "{\"symbols\":[],\"statements\":[[5,\"list-filter\",[],[[],[]]]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "ruKTuHVN",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"list-filter\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        "meta": {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("apchallenge-ember/tests/lint/app.lint-test", [], function () {
  "use strict";

  QUnit.module('ESLint | app');
  QUnit.test('adapters/employee.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/employee.js should pass ESLint\n\n');
  });
  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });
  QUnit.test('components/card-handler.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/card-handler.js should pass ESLint\n\n3:10 - \'set\' is defined but never used. (no-unused-vars)\n6:18 - Do not use the attrs snapshot that is passed in `didReceiveAttrs` and `didUpdateAttrs`. Please see the following guide for more information: https://github.com/ember-cli/eslint-plugin-ember/blob/master/docs/rules/no-attrs-snapshot.md (ember/no-attrs-snapshot)\n6:19 - \'options\' is defined but never used. (no-unused-vars)');
  });
  QUnit.test('components/employee-card.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/employee-card.js should pass ESLint\n\n');
  });
  QUnit.test('components/employees-tree.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/employees-tree.js should pass ESLint\n\n5:10 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n5:10 - \'Ember\' is not defined. (no-undef)\n11:5 - Do not use global `$` or `jQuery` (ember/no-global-jquery)\n11:5 - \'jQuery\' is not defined. (no-undef)\n25:28 - Do not use global `$` or `jQuery` (ember/no-global-jquery)\n25:28 - \'jQuery\' is not defined. (no-undef)\n26:28 - Do not use global `$` or `jQuery` (ember/no-global-jquery)\n26:28 - \'jQuery\' is not defined. (no-undef)');
  });
  QUnit.test('components/list-filter.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/list-filter.js should pass ESLint\n\n');
  });
  QUnit.test('controllers/employees.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/employees.js should pass ESLint\n\n');
  });
  QUnit.test('models/employee.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/employee.js should pass ESLint\n\n');
  });
  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });
  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });
  QUnit.test('routes/employees.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/employees.js should pass ESLint\n\n');
  });
  QUnit.test('serializers/employee.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/employee.js should pass ESLint\n\n');
  });
});
define("apchallenge-ember/tests/lint/templates.template.lint-test", [], function () {
  "use strict";

  QUnit.module('TemplateLint');
  QUnit.test('apchallenge-ember/templates/application.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'apchallenge-ember/templates/application.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('apchallenge-ember/templates/components/card-handler.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'apchallenge-ember/templates/components/card-handler.hbs should pass TemplateLint.\n\napchallenge-ember/templates/components/card-handler.hbs\n  3:16  error  Unnecessary string concatenation. Use {{this.employee.manager_id}} instead of "{{this.employee.manager_id}}".  no-unnecessary-concat\n');
  });
  QUnit.test('apchallenge-ember/templates/components/employee-card.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'apchallenge-ember/templates/components/employee-card.hbs should pass TemplateLint.\n\napchallenge-ember/templates/components/employee-card.hbs\n  1:34  error  Unnecessary string concatenation. Use {{this.employee.id}} instead of "{{this.employee.id}}".  no-unnecessary-concat\n  5:8  error  Self-closing a void element is redundant  self-closing-void-elements\n');
  });
  QUnit.test('apchallenge-ember/templates/components/employees-tree.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'apchallenge-ember/templates/components/employees-tree.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('apchallenge-ember/templates/components/list-filter.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'apchallenge-ember/templates/components/list-filter.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('apchallenge-ember/templates/employees.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'apchallenge-ember/templates/employees.hbs should pass TemplateLint.\n\napchallenge-ember/templates/employees.hbs\n  5:0  error  Incorrect indentation for `<!-- TODO add handler for empty search results -->` beginning at L5:C0. Expected `<!-- TODO add handler for empty search results -->` to be at an indentation of 2 but was found at 0.  block-indentation\n  5:0  error  HTML comment detected  no-html-comments\n  10:2  error  HTML comment detected  no-html-comments\n');
  });
});
define("apchallenge-ember/tests/lint/tests.lint-test", [], function () {
  "use strict";

  QUnit.module('ESLint | tests');
  QUnit.test('integration/components/card-handler-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/card-handler-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/employee-card-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/employee-card-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/employees-tree-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/employees-tree-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/list-filter-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/list-filter-test.js should pass ESLint\n\n');
  });
  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });
  QUnit.test('unit/adapters/employee-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/employee-test.js should pass ESLint\n\n');
  });
  QUnit.test('unit/controllers/employees-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/employees-test.js should pass ESLint\n\n');
  });
  QUnit.test('unit/models/employee-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/employee-test.js should pass ESLint\n\n');
  });
  QUnit.test('unit/routes/employees-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/employees-test.js should pass ESLint\n\n');
  });
  QUnit.test('unit/serializers/employee-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/serializers/employee-test.js should pass ESLint\n\n');
  });
});
define("apchallenge-ember/tests/test-helper", ["apchallenge-ember/app", "apchallenge-ember/config/environment", "@ember/test-helpers", "ember-qunit"], function (_app, _environment, _testHelpers, _emberQunit) {
  "use strict";

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));
  (0, _emberQunit.start)();
});
define("apchallenge-ember/tests/unit/adapters/employee-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Adapter | employee', function (hooks) {
    (0, _emberQunit.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:employee');
      assert.ok(adapter);
    });
  });
});
define("apchallenge-ember/tests/unit/controllers/employees-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Controller | employees', function (hooks) {
    (0, _emberQunit.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:employees');
      assert.ok(controller);
    });
  });
});
define("apchallenge-ember/tests/unit/models/employee-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Model | employee', function (hooks) {
    (0, _emberQunit.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('employee', {});
      assert.ok(model);
    });
  });
});
define("apchallenge-ember/tests/unit/routes/employees-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Route | employees', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:employees');
      assert.ok(route);
    });
  });
});
define("apchallenge-ember/tests/unit/serializers/employee-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Serializer | employee', function (hooks) {
    (0, _emberQunit.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let serializer = store.serializerFor('employee');
      assert.ok(serializer);
    });
    (0, _qunit.test)('it serializes records', function (assert) {
      let store = this.owner.lookup('service:store');
      let record = store.createRecord('employee', {});
      let serializedRecord = record.serialize();
      assert.ok(serializedRecord);
    });
  });
});
define('apchallenge-ember/config/environment', [], function() {
  var prefix = 'apchallenge-ember';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('apchallenge-ember/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
