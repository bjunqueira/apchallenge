import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    filterEmployees (param) {
      if (param !== '') {
          return this.store.query('employee', {
            filter: param
          }).then(function(results) {
            return { query: param, results: results };
          });
      } else {
        return this.store.findAll('employee').then(function(results) {
          return { query: param, results: results };
        });
      }
    },
  }
});
