import DS from 'ember-data';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  manager_id: attr('string'),
  name      : attr('string'),
  phone     : attr('string'),
  mobile    : attr('string'),
  email     : attr('string'),
  picture   : attr('string'),
  address   : attr('string')
});
