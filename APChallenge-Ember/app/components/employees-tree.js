import Component from '@ember/component';
import { set } from '@ember/object';

export default Component.extend({
  store: Ember.inject.service(),

  didRender() {
    this._super(...arguments);

    var store = this.get('store');
    jQuery('.sortable').nestedSortable({
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			isTree: false,
			expandOnHover: 700,
      update: function(event, ui) {
        let employeeId   = jQuery(ui.item[0]).attr('data-id');
        let newManagerId = jQuery(ui.item[0])
          .closest('ol')
          .closest('li')
          .attr('data-id');

        store.findRecord('employee', employeeId).then(
          function(employee) {
            set(employee, "manager_id", newManagerId);
            employee.save();
          }
        );
      }
		});
  }
});
