import Component from '@ember/component';
import jQuery from 'jquery';
import { set } from '@ember/object';

export default Component.extend({
  didReceiveAttrs(options) {
    this._super(...arguments);

    // Check if employee has a manager and manager has already a container rendered.
    if (this.employee.manager_id == undefined
      || (this.employee.manager_id != undefined
        && jQuery('li[data-id='+this.employee.manager_id+']').length
      )
    ){
      // Found a container for the manager, don't create a new one.
      this.create_manager_card = false;
    } else {
      this.create_manager_card = true;
    }
  },
  didInsertElement() {
    this._super(...arguments);
    // If card already was created, we insert the current employee in it.
    if (this.create_manager_card == false) {
      // Manager card is already created. Move employee card inside id.
      if (this.employee.manager_id != undefined) {
        let employeeCard = jQuery('li[data-id='+this.employee.id+']').closest('ol');
        let managerCard  = jQuery('li[data-id='+this.employee.manager_id+']');

        jQuery(managerCard).first().append(employeeCard);
      }
    }
  },
});
