<?php
namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use function Symfony\Component\DependencyInjection\Exception\__toString;

/**
 * The employee repository class.
 */
class EmployeeRepository extends ServiceEntityRepository
{


    /**
     * The constructor.
     *
     * @param RegistryInterface $registry
     *
     * @return void
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Employee::class);

    }//end __construct()


    /**
     * Filter and sort the employees list.
     *
     * @param string $filter The value we want to find in the results.
     * @param string $sort   The column name to order by.
     *
     * @return 
     */
    public function getListFilteredAndSorted($filter = '', $sort = 'name')
    {
        $Query = $this->createQueryBuilder('employees');

        if (!empty($filter)) {
            $Query->orWhere('employees.name LIKE :filter')
            ->orWhere('employees.email LIKE :filter')
            ->orWhere('employees.phone LIKE :filter')
            ->orWhere('employees.mobile LIKE :filter')
            ->setParameter('filter', "%$filter%");
        }

        $employees = $Query
            ->addOrderBy('employees.managerId', 'ASC')
            ->addOrderBy("employees.$sort")
            ->getQuery()
            ->getResult();
        return $employees;

    }//end filter()


}//end class