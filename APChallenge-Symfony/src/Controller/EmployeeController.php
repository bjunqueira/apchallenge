<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use App\Entity\Employee;


/**
 * Employee controller.
 * @Route("/api/v1", name="api_")
 */
class EmployeeController extends FOSRestController
{


    /**
     * Gets a collection of Employees.
     *
     * @Rest\Get("/employees")
     * @QueryParam(name="filter", nullable=true)
     * @QueryParam(name="sort", nullable=true)
     *
     * @return Response
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        // Fetch query params for sorting and filtering the result.
        $filter = $paramFetcher->get('filter');
        // Default sorting by name.
        $sort = $paramFetcher->get('sort') ?? 'name';
        $repository = $this->getDoctrine()->getRepository(Employee::class);

        // Fetch all for a given filter.
        $employees = $repository->getListFilteredAndSorted($filter, $sort);

        $response = $this->handleView($this->view($employees));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;

    }//end getAction()


    /**
     * Get an employee.
     *
     * @Rest\Get("/employees/{id}")
     *
     * @return Response
     */
    public function getAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Employee::class);

        $employee = $repository->findOneBy(array('id' => $id));

        $response = $this->handleView($this->view($employee));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;

    }//end getAction()


    /**
     * Update employees hierarchy.
     *
     * @Rest\Put("/employees/{id}")
     *
     * @return Response
     */
    public function putAction($id, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $response = $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
//         $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;

    }//end putAction()


}//end class