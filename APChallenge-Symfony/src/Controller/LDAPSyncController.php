<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Ldap\Ldap;
use App\Entity\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LDAPSyncController extends AbstractController
{


    /**
     * Pull employees that are not in our database from LDAP.
     *
     * @return Response
     */
    public function pull()
    {
        // TODO move ldap connection logic to a service.
        // TODO verify if employee is already in the DB before inserting.
        try {
            $ldap = Ldap::create('ext_ldap', [
                'host' => 'localhost',
                'port' => '389',
            ]);

            $ldap->bind('cn=admin,dc=ecorp,dc=org', 'supersecret');
            $query   = $ldap->query('ou=people,dc=ecorp,dc=org', '(uid=*)');
            $results = $query->execute();

            $entityManager = $this->getDoctrine()->getManager();

            foreach ($results as $entry) {
                // Create new employee and set its attributes from what we received from ldap query.
                $Employee = new Employee();
                $Employee->setId($entry->getAttribute('uid')[0]);
                $Employee->setName($entry->getAttribute('cn')[0]);
                $Employee->setEmail($entry->getAttribute('mail')[0]);
                $Employee->setMobile($entry->getAttribute('mobile')[0]);
                $Employee->setPhone($entry->getAttribute('telephoneNumber')[0]);
                $Employee->setPicture(base64_encode($entry->getAttribute('jpegPhoto')[0]));
                // Check whether the employee has a manager.
                if ($entry->hasAttribute('manager')) {
                    // Grab only the manager id from the ldap attribute.
                    $managerId = $entry->getAttribute('manager')[0];
                    // Here is the manager attribute the way we receive: 'uid=55,ou=People,dc=ecorp,dc=org'.
                    $managerId = substr($managerId, 4, strpos($managerId, ',')-4);
                    $Employee->setManagerId((int)$managerId);
                }
                $entityManager->persist($Employee);
            }
            $entityManager->flush();
        } catch (\Exception $E) {
            return new Response($E->getMessage(), 500);
        }

        return new Response('success');

    }//end pull()


    /**
     * Push latest changes to LDAP database.
     *
     * @return Response
     */
    public function push()
    {
        // TODO

    }//end push()


}//end class